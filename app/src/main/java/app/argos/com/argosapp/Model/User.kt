package app.argos.com.argosapp.Model

/**
 * Created by charlotte on 08.05.2019.
 */
class User(id: Int?, email: String?, cellphone: String?) {

    var id: Int? = id
    var email: String? = email
    var cellphone: String? = cellphone

}