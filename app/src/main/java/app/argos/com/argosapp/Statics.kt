package app.argos.com.argosapp

/**
 * Created by charlotte on 08.05.2019.
 */

object Statics {
    @JvmStatic val API_TOKEN: String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF91c2VyIjo1LCJpYXQiOjE1NTg4ODY1MzgsImV4cCI6MTU1ODk3MjkzOH0.O1_I9pEV3Wbzh1S_yuRGnIunPidl0mRD61rWQo2lIvs"
    @JvmStatic var IS_CONNECTED: Boolean = false
}